TypeScript (+Node.js) with VSCode debug starter.
================================================

# How to make this project

1. make project directory.

```bash
$ make tsnodebase
$ cd tsnodebase
```

2. init project

```bash
$ npm init --yes
```

3. change package.json to "module".

```
"main": "index"
```
to
```
"main": "index"
"type": "module"
```

4. install TypeScript.

```bash
$ npm install --save-dev typescript @types/node
```

5. init tsc

```bash
$ npx tsc --init
```

6. change tsconfig.json.

```
"target": "es2016"
```
to
```
"target": "es2020"
```

```
"module": "commonjs"
```
to
```
"module": "esnext"
```

```
// "moduleResolution": "node"
```
to
```
"moduleResolution": "node"
```

```
// "outDir": "./"
```
to
```
"outDir": "./dist"
```

add "sourceMap" in "compilerOptions".
```
{
  "compilerOptions": {
    ...
    "skipLibCheck": true
  }
}
```
to
```
{
  "compilerOptions": {
    ...
    "skipLibCheck": true,
    "sourceMap": true,
  }
}
```

add "include" after "compilerOptions".
```
{
  "compilerOptions": {
    ...
  }
}
```
to
```
{
  "compilerOptions": {
    ...
  },
  "include": ["./src/**/*.ts"]
}
```

7. make .gitignore

```
dist
node_modules
package-lock.json
```

8. make .vscode/launch.json

```
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "Launch Program",
            "program": "${workspaceFolder}/dist/index.js",
            "preLaunchTask": "build"
       }
    ]
}
```

9. make .vscode/tasks.json

```
{
    version": "2.0.0",
    tasks": [
        {
            "label": "build",
            "type": "typescript",
            "tsconfig": "tsconfig.json",
            "problemMatcher": [
                "$tsc"
            ],
            "group": {
            "kind": "build",
                "isDefault": true
            }
        }
    ]
}
```

9. make src/index.ts

```
const message: string = "Hello, world!";

console.log(message);
```

10. open project directory with VSCode.

11. open src/index.ts in left tree.

12. "Run" -> "Run with debug".

